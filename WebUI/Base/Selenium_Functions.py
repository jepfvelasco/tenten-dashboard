from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from Utility.cutom_logger import customLogger
import logging
import time
import os

'''
class containing user-customized Selenium methods
'''
class SeleniumFX():
    logger = customLogger(logging.DEBUG)

    def __init__(self, driver):
        self.driver = driver

    #method to locate element using tuple locators. Returns element
    def getElement(self,locator):
        try:
            el = self.driver.find_element(*locator)
            self.logger.info("Found element " + str(locator))
            return el
        except:
            self.logger.error('Element not found: ' + str(locator))

    # method to locate element using specific locator inside test method and locator type. Returns element
    def getElementlocandtype(self,locatortype,locator):
        try:
            el = self.driver.find_element(locatortype, locator)
            self.logger.info("Found element " + str(locator))
            return el
        except:
            self.logger.error('Eement not found: ' + str(locator))

    # method to locate element using tuple locators using tuple locators. Returns list of elements
    def getElements(self,locator):
        try:
            els = self.driver.find_elements(*locator)
            self.logger.info("Found elements " + str(locator))
            return els
        except:
            self.logger.error('Eements not found: ' + str(locator))

    # method to wait for element presence using tuple locators. Returns element
    def waitforELement(self, locator):
        try:
            el = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(locator))
            self.logger.info("Found element " + str(locator))
            return el
        except:
            self.logger.error('Element not found: ' + str(locator))

    # method to check presence of element using tuple locator. Returns bool
    def checkElementPresence(self,locator):
        try:
            el = self.driver.find_element(*locator)
            if el is not None:
                self.logger.info("Element present " + str(locator))
                return True
            else:
                return False
        except:
            self.logger.info('Element not found: ' + str(locator))

    # method ot check is element can be clicked. Returns bool
    def elementisClickable(self,locator):
        try:
            el = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable(locator))
            self.logger.info("Element clickable " + str(locator))
            return True
        except:
            self.logger.info('Element not clickable: ' + str(locator))
            return False

    # method to click an element using custom getElement method above
    def clickElement(self, locator, el=None):
        try:
            if el == None:
                el = self.getElement(locator)
            el.click()
            self.logger.info('Clicked element: ' + str(locator) + str(el))
        except:
            self.logger.error('Cannot click element: ' + str(locator) + str(el))

    # method that waits and clicks an element using custom waitforElement method above
    def waitandclickElement(self, locator, el=None):
        try:
            if el == None:
                el = self.waitforELement(locator)
            el.click()
            self.logger.info('Clicked element: ' + str(locator) + str(el))
        except:
            self.logger.error('Cannot click element: ' + str(locator) + str(el))

    # method to send string/value to an element using custom getElement method above
    def sendData(self, data, locator, el=None):
        try:
            self.clearText(locator=locator,el=el)
            if el == None:
                el = self.getElement(locator)
            el.send_keys(data)
            self.logger.info('Sent data to element: ' + str(locator) + str(el))
        except:
            self.logger.error('Cannot send data to element: ' + str(locator) + str(el))

    # method to send string/value to an element using custom waitforElement method above
    def waitandsendData(self, data, locator, el=None):
        try:
            self.clearText(locator=locator, el=el)
            if el == None:
                el = self.waitforELement(locator)
            el.send_keys(data)
            self.logger.info('Sent data to element: ' + str(locator) + str(el))
        except:
            self.logger.error('Cannot send data to element: ' + str(locator) + str(el))

    # method to clear a text field using custom getElement method above
    def clearText(self, locator, el=None):
        try:
            if el==None:
                el = self.getElement(locator)
            el.send_keys(Keys.CONTROL+"a")
            el.send_keys(Keys.DELETE)
            self.logger.info('Text cleared on: ' + str(locator) + str(el))
        except:
            self.logger.error('Text not cleared on element: ' + str(locator) + str(el))
