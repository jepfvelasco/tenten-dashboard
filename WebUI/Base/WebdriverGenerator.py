from selenium import webdriver
'''
Class that generates webdriver instance based on arguments provided to pytest
'''
class WebDriverGen():

    #init method
    def __init__(self, browser):
        self.browser = browser

    # method that creates webdriver instance. Returns webdriver
    def getWebdriverInstance(self):
        baseURL = 'https://dashboard-78fa7.web.app/login'
        if self.browser == 'iexplorer':
            driver = webdriver.Ie()
        elif self.browser == 'chrome':
            driver = webdriver.Chrome()
        elif self.browser == 'firefox':
            driver = webdriver.Firefox()
        else:
            driver = webdriver.Chrome()

        driver.implicitly_wait(3)
        #driver.maximize_window()
        driver.get(baseURL)

        return driver

