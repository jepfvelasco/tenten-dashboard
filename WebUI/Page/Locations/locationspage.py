from WebUI.Base.Selenium_Functions import SeleniumFX
from selenium.webdriver.common.by import By
from WebAPI.Base.API_Verbs import API_verbs
from Utility.cutom_logger import customLogger
import logging
'''
page class for Locations page
'''
class LocationsPage(SeleniumFX):
    #logger
    logger = customLogger(logging.DEBUG)

    #locators
    _addlocation_button = (By.XPATH, "//button[text()='Add Location']")
    _first_page = (By.XPATH,"//button[@aria-label='first page']")
    _prev_page = (By.XPATH, "//button[@aria-label='previous page']")
    _next_page = (By.XPATH, "//button[@aria-label='next page']")
    _last_page = (By.XPATH, "//button[@aria-label='last page']")
    _table = "//form/table/tbody/tr/td"
    _tablerow = (By.XPATH,"//form/table/tbody/tr")
    _tablecols = (By.XPATH,"//form/table/tbody/tr[1]/td")
    _save = (By.XPATH,"//button[text()='Save']")
    _cancel = (By.XPATH, "//button[text()='Cancel']")
    _add_loc_latitude = (By.XPATH,"//input[@name='latitude']")
    _add_loc_longitude = (By.XPATH, "//input[@name='longitude']")
    _edit_loc_latitude = (By.NAME, 'attributes.latitude')
    _edit_loc_longitude = (By.NAME, 'attributes.longitude')
    _count = (By.XPATH,"//span[contains(text(),'-') and contains(text(),'of') ]")
    # _latitude = [By.XPATH, "//td[text()='%s']/../td[3]"]
    # _longitude = [By.XPATH, "//td[text()='%2']/../td[2]"]
    # _vending_machine = [By.XPATH, f"//td[text()='%s']"]

    # static_variables
    user_id = "974bfbdf-6d10-46e0-bac3-a39f7514a0a7"

    # method for clicking "add location" button
    def open_addlocation(self):
        self.waitandclickElement(self._addlocation_button)
        self.logger.info("Opened Add Locations")

    # method for sending longitude information to "add location" longitude field
    def add_longitude(self, longitude):
        self.waitandclickElement(self._add_loc_longitude)
        self.waitandsendData(longitude,self._add_loc_longitude)
        self.logger.info("Add input longitude")

    # method for sending latitude information to "add location" latitude field
    def add_latitude(self, latitude):
        self.waitandclickElement(self._add_loc_latitude)
        self.waitandsendData(latitude, self._add_loc_latitude)
        self.logger.info("Add input latitude")

    # method for pressing save button in "add location" and "edit" fields
    def press_save(self):
        self.waitandclickElement(self._save)
        self.logger.info("Save button pressed")

    # method for pressing cancel button in "add location" and "edit" fields
    def press_cancel(self):
        self.waitandclickElement(self._cancel)
        self.logger.info("Cancel button pressed")

    # method for pressing next page button in locations table
    def go_next(self):
        if self.elementisClickable(self._next_page):
            self.clickElement(self._next_page)
            self.logger.info("Next page pressed")

    # method for pressing previous page button in locations table
    def go_previous(self):
        if self.elementisClickable(self._next_page):
            self.clickElement(self._next_page)
            self.logger.info("Previoues page pressed")

    # method for pressing first page button in locations table
    def go_firstpage(self):
        if self.elementisClickable(self._first_page):
            self.clickElement(self._first_page)
            self.logger.info("First page pressed")

    # method for pressing last page button in locations table
    def go_lastpage(self):
        if self.elementisClickable(self._last_page):
            self.clickElement(self._last_page)
            self.logger.info("Last page pressed")

    # method for acquiring all available vending machine information. Returns nested list
    def get_all_vending_mechines(self):
        self.go_firstpage()
        tablerows = len(self.getElements(self._tablerow))
        tablecols = len(self.getElements(self._tablecols))
        vending_machine_list = []
        while self.elementisClickable(self._next_page):
            for i in range(tablerows):
                details = []
                for j in range(tablecols-1):
                    if j == 0:
                        details.append(self.getElementlocandtype(By.XPATH,f"//form/table/tbody/tr[{i+1}]/td[{j+1}]").text)
                    else:
                        details.append(self.getElementlocandtype(By.XPATH,f"//form/table/tbody/tr[{i+1}]/td[{j+1}]/div").text)
                vending_machine_list.append(details)
            self.go_next()
        self.logger.info("Acquired all vending machine info")
        return vending_machine_list

    # method for editing then cancelling specific vending machine based on provided vending_machine id
    def edit_vending_machine(self, vending_machine_id, longitude, latitude):
        _edit = (By.XPATH,f"//td[text()='{vending_machine_id}']/../td[4]//button[1]")
        _vending_machine = (By.XPATH, f"//td[text()='{vending_machine_id}']")
        self.go_firstpage()
        while True:
            if self.checkElementPresence(_vending_machine):
                self.clickElement(_edit)
                self.sendData(longitude, self._edit_loc_longitude)
                self.sendData(latitude, self._edit_loc_latitude)
                self.logger.info("Edited vending machine: " + vending_machine_id)
                break
            if not self.elementisClickable(self._next_page):
                self.logger.error("Vending machine does not exist: " + vending_machine_id)
                return -1
            self.go_next()


    # method for deleting specific vending machine based on provided vending_machine id
    def delete_vending_machine(self, vending_machine_id):
        _delete = (By.XPATH,f"//td[text()='{vending_machine_id}']/../td[4]//button[2]")
        _vending_machine = (By.XPATH, f"//td[text()='{vending_machine_id}']")
        self.go_firstpage()
        while True:
            if self.checkElementPresence(_vending_machine):
                self.clickElement(_delete)
                self.logger.info("Deleted vending machine: " + vending_machine_id)
                break
            if not self.elementisClickable(self._next_page):
                self.logger.error("Vending machine does not exist: " + vending_machine_id)
                return -1
            self.go_next()

    # method for acquiring longitude and latitude information of specific vending machine based on provided vending_machine_id
    def get_longitude_latitude(self, vending_machine_id):
        _latitude = (By.XPATH, f"//td[text()='{vending_machine_id}']/../td[3]")
        _longitude = (By.XPATH, f"//td[text()='{vending_machine_id}']/../td[2]")
        _vending_machine = (By.XPATH, f"//td[text()='{vending_machine_id}']")
        self.go_firstpage()
        while True:
            if self.checkElementPresence(_vending_machine):
                longitude = self.getElement(_longitude).text
                latitude = self.getElement(_latitude).text
                self.logger.info("Acquired longitude: " + vending_machine_id + " : " + longitude)
                self.logger.info("Acquired latitude: " + vending_machine_id + " : " + latitude)
                return longitude, latitude
            if not self.elementisClickable(self._next_page):
                self.logger.error("Vending machine does not exist: " + vending_machine_id)
                return -1
            self.go_next()

    # method for acquiring longitude and latitude information of specific vending machine based on provided vending_machine_id on current page
    def get_longitude_latitude_current_page(self, vending_machine_id):
        _latitude = (By.XPATH, f"//td[text()='{vending_machine_id}']/../td[3]")
        _longitude = (By.XPATH, f"//td[text()='{vending_machine_id}']/../td[2]")
        _vending_machine = (By.XPATH, f"//td[text()='{vending_machine_id}']")
        if self.checkElementPresence(_vending_machine):
            longitude = self.waitforELement(_longitude).text
            latitude = self.waitforELement(_latitude).text
            self.logger.info("Acquired longitude: " + vending_machine_id + " : " + longitude)
            self.logger.info("Acquired latitude: " + vending_machine_id + " : " + latitude)
            return longitude, latitude
        if not self.elementisClickable(self._next_page):
            self.logger.error("Vending machine does not exist: " + vending_machine_id)
            return -1

    def get_count_total(self):
        count = self.waitforELement(self._count).text
        return int(count[-1])

    # method for adding then saving location
    def addlocation(self, longitude, latitude):
        self.open_addlocation()
        self.add_longitude(longitude)
        self.add_latitude(latitude)

    # method to verify added location using api
    def verify_added_location_ui(self, longitude, latitude):
        pass


    # method to verify added location using ui
    def verify_added_location_api(self, longitude, latitude):
        av = API_verbs()
        result, body = av.get_vending_machine(self.user_id)
        for v in body['data']:
            if float(v['attributes']['longitude']) == float(longitude) and float(v['attributes']['latitude']) == float(latitude):
                return True
        return False

    def verify_edited_location_ui(self, vending_machine_id):
        _edit = (By.XPATH, f"//td[text()='{vending_machine_id}']/../td[4]//button[1]")
        if self.waitforELement(_edit) is not None:
            return self.get_longitude_latitude_current_page(vending_machine_id)

    # method to verify added location using ui
    def verify_deleted_location_api(self, vending_machine_id):
        av = API_verbs()
        result, body = av.get_vending_machine(self.user_id)
        for v in body['data']:
            if v['id'] == vending_machine_id:
                return False
        return True



