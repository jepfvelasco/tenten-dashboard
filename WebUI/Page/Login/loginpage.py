from selenium.webdriver.common.by import By
from WebUI.Base.Selenium_Functions import SeleniumFX
from Utility.cutom_logger import customLogger
import logging

'''
page class for Login page
'''
class LoginPage(SeleniumFX):
    #logger
    logger = customLogger(logging.DEBUG)

    #locators
    _email = (By.NAME, 'email')
    _password = (By.NAME, 'password')
    _submitBtn = (By.XPATH,'//button[text()="Submit"]')
    _dashboard = (By.XPATH, '//span[text()="Dashboard"]')
    _loginheader = (By.XPATH, '//h1[text()="Login"]')
    _aftersuccessfulllogin = (By.XPATH, "//span[text()='Dashboard']")

    # method for sending email input to email field
    def sendEmail(self, email):
        self.sendData(data=email, locator=self._email)
        self.logger.info("Sent email: " + email)

    # method for sending password input to password field
    def sendPassword(self, password):
        self.sendData(data=password, locator=self._password)
        self.logger.info("Sent password: " + password)

    # method for pressing login button
    def clickSubmit(self):
        self.clickElement(locator=self._submitBtn)
        self.logger.info("Clicked submit")

    # method for login operation
    def login(self, email, password):
        self.sendEmail(email)
        self.sendPassword(password)
        self.clickSubmit()

    #method to verify login state
    def verifylogin(self):
        el = self.waitforELement(self._aftersuccessfulllogin)
        if el is not None:
            return True
        else:
            return False

    #method to verify failed login state
    def verifyfailedlogin(self):
        el = self.waitforELement(self._aftersuccessfulllogin)
        if el is not None:
            return False
        else:
            return True


