from WebUI.Base.Selenium_Functions import SeleniumFX
from selenium.webdriver.common.by import By

'''
page class for common elements in webpage
'''
class CommonElements(SeleniumFX):
    #locators
    _menudisplay = (By.XPATH,"//span[@class='MuiIconButton-label']")
    _header = (By.XPATH, "//h1[text()='Demo']")
    _dashboardlink = (By.XPATH, "//a[@href='/dashboard']")
    _locationslink = (By.XPATH, "//a[@href='/locations']")

    # method for hiding/displaying dashboad
    def hidedisplay_menu(self):
        self.waitandclickElement(self._menudisplay)

    # method for opening dashboad page
    def open_Dashboard(self):
        self.waitandclickElement(self._dashboardlink)

    # method for opening locations page
    def open_Locations(self):
        self.waitandclickElement(self._locationslink)


