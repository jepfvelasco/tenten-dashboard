from WebUI.Base.Selenium_Functions import SeleniumFX
from selenium.webdriver.common.by import By
from WebAPI.Base.API_Verbs import API_verbs

'''
page class for Dashboard page
'''
class DashboardPage(SeleniumFX):
    #locators
    _vendingmachinescount1 = (By.XPATH, "//div[text()='Vending Machines']//*[local-name() = 'svg']//*[local-name() = 'text']")
    _vendingmachinescount2 = (By.XPATH, "//div[text()='Vending Machines']//*text")
    _map = (By.XPATH, "//div[@id='map']")

    # static_variables
    user_id = "974bfbdf-6d10-46e0-bac3-a39f7514a0a7"

    # method for acquiring number of vending machine displayed in dashboard. Returns string
    def get_VendingMachineCount(self):
        return self.waitforELement(self._vendingmachinescount1).text

    def verify_VendingMachineCount(self):
        self.api = API_verbs()
        return self.api.get_vending_machine_total(user_id=self.user_id)


