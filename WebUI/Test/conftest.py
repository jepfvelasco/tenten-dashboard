import pytest
from WebUI.Base.WebdriverGenerator import WebDriverGen

@pytest.fixture(scope="class")
def SetUp(request, getbrowser):
    wb = WebDriverGen(getbrowser)
    driver = wb.getWebdriverInstance()

    if request.cls is not None:
        request.cls.driver = driver
    yield driver
    driver.quit()

def pytest_addoption(parser):
    parser.addoption("--browser", help="Type browser to be used for testing. ('iexplorer','chrome','firefox')")

@pytest.fixture(scope="session")
def getbrowser(request):
    return request.config.getoption("--browser")

