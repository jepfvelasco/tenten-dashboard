import time
import pytest
import unittest
from WebUI.Page.Locations.locationspage import LocationsPage
from WebUI.Page.Login.loginpage import LoginPage
from WebUI.Page.Common.common import CommonElements
from Utility.custom_assert import Custom_Assert

@pytest.mark.usefixtures('SetUp')
class LocationTests(unittest.TestCase):

    # fixture that runs once for whole test class
    @pytest.fixture(autouse=True, scope='class')
    def classSetup(self, SetUp):
        self.lp = LoginPage(self.driver)
        self.lp.login('demo@mytenten.com','demo123!')
        self.co = CommonElements(self.driver)
        self.co.open_Locations()


    # Fixture that runs once for each test case
    @pytest.fixture(autouse=True)
    def objectSetup(self, SetUp):
        self.locpage = LocationsPage(self.driver)
        self.ca = Custom_Assert(self.driver)


    # Method to test adding of vending machine (Save)
    def test_AddVendingMachine_save(self):
        inputs = ('30.0', '30.0')
        previous_total = self.locpage.get_count_total()
        self.locpage.addlocation(*inputs)
        self.locpage.press_save()
        assertion1, message1 = self.ca.custom_assert(self.locpage.verify_added_location_api(*inputs), True)
        assertion2, message2 = self.ca.custom_assert(self.locpage.get_count_total(), previous_total + 1)
        assert (assertion1 == assertion2 == True), message1 + " : " + message2
        # assert self.locpage.verify_added_location_api(*inputs) == True
        # assert self.locpage.get_count_total() == previous_total+1

    # Method to test adding of vending machine (Cancel)
    def test_AddVendingMachine_cancel(self):
        inputs = ('30.0', '30.0')
        previous_total = self.locpage.get_count_total()
        self.locpage.addlocation(*inputs)
        self.locpage.press_cancel()
        assertion1, message1 = self.ca.custom_assert(self.locpage.verify_added_location_api(*inputs), False)
        assertion2, message2 = self.ca.custom_assert(self.locpage.get_count_total(), previous_total)
        assert (assertion1 == assertion2 == True), message1 + " : " + message2
        #assert self.locpage.verify_added_location_api(*inputs) == False
        #assert self.locpage.get_count_total() == previous_total

    # Method to test editing of vending machine (Save)
    def test_editvendingmachine_save(self):
        inputs = ('aa16672a-557d-4eaa-b94c-6cfe97aa6b33','-151', '151')
        if self.locpage.edit_vending_machine(*inputs) == -1:
            assert False, "Vending Machine ID does not exist"
        else:
            self.locpage.press_save()
            time.sleep(1)
            longitude, latitude = self.locpage.verify_edited_location_ui(inputs[0])
            assert (float(longitude), float(latitude)) == (float(inputs[1]), float(inputs[2]))

    # Method to test editing of vending machine (Cancel)
    def test_editvendingmachine_cancel(self):
        inputs = ('aa16672a-557d-4eaa-b94c-6cfe97aa6b33','0','0')
        if self.locpage.edit_vending_machine(*inputs) == -1:
            assert False, "Vending Machine ID does not exist"
        else:
            self.locpage.press_cancel()
            longitude, latitude = self.locpage.verify_edited_location_ui(inputs[0])
            assert (float(longitude), float(latitude)) != (float(inputs[1]), float(inputs[2]))

    # Method to test deleting of vending machine (Save)
    def test_deletevendingmachine(self):
        inputs = ('589e4bf8-bb47-49c7-9e40-9f145419e805')
        if self.locpage.delete_vending_machine(inputs) == -1:
            assert False, "Vending Machine ID does not exist"
        else:
            assert self.locpage.verify_deleted_location_api(inputs) == True





