from WebUI.Page.Login.loginpage import LoginPage
from Utility.data_reader import getdata
from ddt import ddt, data, unpack
import unittest
import pytest

@ddt
@pytest.mark.usefixtures('SetUp')
class LoginTests(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self):
        self.pl = LoginPage(self.driver)

    # method for testing invalid login using data driven approach
    @pytest.mark.run(order=2)
    @data(*getdata('testdata_Invalid_Login.csv'))
    @unpack
    def test_invalid_Login(self, email, password):
        self.pl.login(email, password)
        assert self.pl.verifyfailedlogin() == True

    # method for testing valid login using data driven approach
    @pytest.mark.run(order=3)
    @data(*getdata('testdata_Valid_Login.csv'))
    @unpack
    def test_valid_Login(self, email, password):
        self.pl.login(email, password)
        assert self.pl.verifylogin() == True