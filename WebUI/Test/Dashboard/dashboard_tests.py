from WebUI.Page.Dashboard.dashboardpage import DashboardPage
from WebUI.Page.Login.loginpage import LoginPage
import unittest
import pytest
from WebAPI.Base.API_Verbs import API_verbs

@pytest.mark.usefixtures('SetUp')
class DashboardTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self, SetUp):
        self.dp = DashboardPage(self.driver)
        self.lp = LoginPage(self.driver)
        self.lp.login('demo@mytenten.com','demo123!')
        self.api = API_verbs()

    def test_VendingMachineCount(self):
        ui_count = self.dp.get_VendingMachineCount()
        api_count = self.dp.verify_VendingMachineCount
        assert ui_count == api_count, str(ui_count) + "==" + str(api_count)



