import unittest
from WebUI.Test.Login.login_tests import LoginTests
from WebUI.Test.Dashboard.dashboard_tests import DashboardTest
from WebUI.Test.Locations.location_tests import LocationTests

test1 = unittest.TestLoader().loadTestsFromTestCase(LoginTests)
test2 = unittest.TestLoader().loadTestsFromTestCase(DashboardTest)
test3 = unittest.TestLoader().loadTestsFromTestCase(LocationTests)

test_suite1 = unittest.TestSuite([test1, test2, test3])


unittest.TextTestRunner(verbosity=2).run(test_suite1)
