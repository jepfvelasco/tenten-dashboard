# README #

Setup
1. Download test package
2. Install python in local environment
    - Download: https://www.python.org/downloads/
    - On first screen of installation wizard: Check "Add Python X.X to PATH" during installation
3. Extract test package
4. Using command prompt navigate to extracted folder (where requirements.txt is located)
5. Install requirements
    - pip install -r requirements.txt
6. Download browser drivers
    - chromedriver: https://chromedriver.chromium.org/downloads
    - firefoxdriver: https://github.com/mozilla/geckodriver/releases
    - iexplorer: https://selenium-release.storage.googleapis.com/index.html
             - win32 for iexplorer (whichever latest version is available)
7. Extract all browser drivers to a single location
8. Add browser drivers location to system path
    - Open "Edit system environment variables" in windows
    - Click Environment Variables> Environment Variables> 
    - Edit "path"
    - Add browser drivers location

WebUI tests
1. Using command prompt navigate to test package folder, go to WebUI\Test\
2. To test all available test cases, run the following command:
    - py.test -ra --browser <browser choice> --html==test_suite_report.html
3. After the test, report will be generated in command prompt and an html file in the directory
  
Notes: 
individual test can be ran inside specific folders WebUI\Test\ using the same command
test data for login use a separate csv file to show data-driven testing
test data for other tests are fed directly in the test methods
some test might file/pass depending on the test data and actual data available in the WebUI

Available Tests:
 - Invalid Login
 - Valid Login
 - Dashboard: Verify number of vending machines 
 - Locations: Verify Add, Edit, Delete vending machines

WebAPI tests
1. Using command prompt navigate to test package folder, go to WebAPI\Test\
2. To test all available test cases, run the following command:
    - py.test -ra --html=test_suite_report.html
3. After the test, report will be generated in command prompt and an html file in the directory

  
Notes: 
individual test can be ran inside specific folders WebAPI\Test\ using the following command:
    - py.test <test file name>
test data for all use a separate csv file to show data-driven testing
some test might file/pass depending on the test data and actual data available in the WebUI (i.e. vending machine id input may already be not available)

Available Tests:
 - get
 - post
 - delete

