from WebAPI.Base.API_Verbs import API_verbs
import pytest
import unittest
from ddt import ddt, data, unpack
from Utility.data_reader import getdata

@ddt
class Test_Delete_API(unittest.TestCase):

    #fixture that runs once before/after all methods
    @pytest.fixture(autouse=True)
    def Setup(self):
        self.av = API_verbs()

    #test method 1 u(data driven)
    @data(*getdata('Test_data/delete_testdata.csv'))
    @unpack
    def test_delete_vending_machine(self, user_id, vending_machine_id, expected_result):
        result, body = self.av.delete_vending_machine(user_id=user_id, vending_machine_id=vending_machine_id)
        if result == 200 and vending_machine_id != '':
            vid = body['data']['id']
            assert vid == vending_machine_id
        assert result == int(expected_result)