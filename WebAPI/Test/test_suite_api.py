import unittest
from WebAPI.Test.get_tests import Test_Get_API
from WebAPI.Test.post_tests import Test_Post_API
from WebAPI.Test.delete_tests import Test_Delete_API

test1 = unittest.TestLoader().loadTestsFromTestCase(Test_Get_API)
test2 = unittest.TestLoader().loadTestsFromTestCase(Test_Post_API)
test3 = unittest.TestLoader().loadTestsFromTestCase(Test_Delete_API)

test_suite1 = unittest.TestSuite([test1, test2, test3])


unittest.TextTestRunner(verbosity=2).run(test_suite1)
