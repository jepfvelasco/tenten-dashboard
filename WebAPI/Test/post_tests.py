from WebAPI.Base.API_Verbs import  API_verbs
import pytest
from Utility.data_reader import getdata
from ddt import ddt, data, unpack
import unittest

@ddt
class Test_Post_API(unittest.TestCase):

    # fixture that runs once before/after all methods
    @pytest.fixture(autouse=True)
    def Setup(self):
        self.av = API_verbs()

    # test method 1 u(data driven)
    @data(*getdata('./Test_data/post_testdata.csv'))
    @unpack
    def test_post_vending_machine(self, user_id, longitude, latitude, expectedresult):
        result, body = self.av.post_vending_machine(user_id=user_id, longitude_val=longitude, latitude_val=latitude)
        print(result)
        print(body)
        if result == 201:
            assert float(body['data']['attributes']['latitude']) == float(latitude)
            assert float(body['data']['attributes']['longitude']) == float(longitude)
        assert result == int(expectedresult)


    # def test_post_vendo_404(self):
    #     result, body = self.post_vendo(self.invalid_user_id, longitude_val=140, latitude_val=40)
    #     if result == 201:
    #         print(body['data']['id'])
    #         print(body['data']['attributes']['latitude'])
    #         print(body['data']['attributes']['longitude'])
    #     assert result == 404
    #
    # def test_post_vendo_422_1(self):
    #     result, body = self.post_vendo(self.valid_user_id, longitude_val=181, latitude_val=0)
    #     if result == 201:
    #         print(body['data']['id'])
    #         print(body['data']['attributes']['latitude'])
    #         print(body['data']['attributes']['longitude'])
    #     assert result == 422
    #
    # def test_post_vendo_422_2(self):
    #     result, body = self.post_vendo(self.valid_user_id, longitude_val=-181, latitude_val=0)
    #     if result == 201:
    #         print(body['data']['id'])
    #         print(body['data']['attributes']['latitude'])
    #         print(body['data']['attributes']['longitude'])
    #     assert result == 422
    #
    # def test_post_vendo_422_3(self):
    #     result, body = self.post_vendo(self.valid_user_id, longitude_val=0, latitude_val=91)
    #     if result == 201:
    #         print(body['data']['id'])
    #         print(body['data']['attributes']['latitude'])
    #         print(body['data']['attributes']['longitude'])
    #     assert result == 422
    #
    # def test_post_vendo_422_4(self):
    #     result, body = self.post_vendo(self.valid_user_id, longitude_val=-181, latitude_val=-91)
    #     if result == 201:
    #         print(body['data']['id'])
    #         print(body['data']['attributes']['latitude'])
    #         print(body['data']['attributes']['longitude'])
    #     assert result == 422




