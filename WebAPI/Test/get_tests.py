from WebAPI.Base.API_Verbs import API_verbs
import pytest
import unittest
from ddt import ddt, data, unpack
from Utility.data_reader import getdata

@ddt
class Test_Get_API(unittest.TestCase):
    # fixture that runs once before/after all methods
    @pytest.fixture(autouse=True)
    def Setup(self):
        self.av = API_verbs()

    @data(*getdata('Test_data\get_testdata.csv'))
    @unpack
    def test_get_vending_machine(self, user_id, vending_machine_id, expected_result):
        result, body = self.av.get_vending_machine(user_id=user_id, vending_machine_id=vending_machine_id)
        if result == 200 and vending_machine_id != '':
            vid = body['data']['id']
            print("vending machine id: " + vid)
            assert vid == vending_machine_id
        assert result == int(expected_result)

    # @data(*getdata('Test_data/get_testdata.csv'))
    # @unpack
    # def test_get_vending_machine(self, user_id, vending_machine_id, expected_result):
    #     result, body = self.av.get_vending_machine(user_id=user_id, vending_machine_id=vending_machine_id)
    #     if result == 200:
    #         if vending_machine_id=='':
    #             for v in range(len(body['data'])):
    #                 pass
    #                 #print(body['data'][v]['id'])
    #                 #print(body['data'][v]['attributes']['latitude'])
    #                 #print(body['data'][v]['attributes']['longitude'])
    #         else:
    #             vid = body['data']['id']
    #             #print(body['data']['attributes']['latitude'])
    #             #print(body['data']['attributes']['longitude'])
    #             assert vid == vending_machine_id
    #     assert result == int(expected_result)
    #