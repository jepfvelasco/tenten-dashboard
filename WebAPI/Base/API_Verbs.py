import requests
import json

class API_verbs():
    def get_vending_machine(self, user_id, vending_machine_id=""):
        uri = f'http://frontend-test-api.herokuapp.com/api/v1/users/{user_id}/vending_machines/{vending_machine_id}'
        resp = requests.get(uri)
        resp_body = json.loads(resp.text)
        return resp.status_code, resp_body

    def post_vending_machine(self, user_id, longitude_val, latitude_val, vendo_id=""):
        headers = {'Content-type':'application/json'}
        payload = {'vending_machine': {'longitude': longitude_val, 'latitude': latitude_val}}
        uri = f'http://frontend-test-api.herokuapp.com/api/v1/users/{user_id}/vending_machines/{vendo_id}'
        resp = requests.post(uri, headers=headers, data=json.dumps(payload,indent=4))
        resp_body = json.loads(resp.text)
        return resp.status_code, resp_body

    def delete_vending_machine(self, user_id, vending_machine_id=""):
        uri = f'http://frontend-test-api.herokuapp.com/api/v1/users/{user_id}/vending_machines/{vending_machine_id}'
        resp = requests.delete(uri)
        resp_body = json.loads(resp.text)
        return resp.status_code, resp_body

    def get_vending_machine_total(self, user_id):
        uri = f'http://frontend-test-api.herokuapp.com/api/v1/users/{user_id}/vending_machines/'
        resp = requests.get(uri)
        resp_body = json.loads(resp.text)
        total = len(resp_body['data'])
        return total