import csv

'''
utility method for reading test data (csv files)
'''
def getdata(filename):
    rows = []
    file = open(filename,'r')
    reader = csv.reader(file)
    next(reader)
    for row in reader:
        rows.append(row)
    return rows