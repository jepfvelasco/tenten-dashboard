from WebUI.Base.Selenium_Functions import SeleniumFX

class Custom_Assert(SeleniumFX):
    def custom_assert(self, arg1, arg2):
        if arg1 == arg2:
            return True, str(arg1) + "==" + str(arg2)
        else:
            return False, str(arg1) + "==" + str(arg2)
